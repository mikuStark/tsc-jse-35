package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.model.AbstractEntity;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;

import java.util.List;
import java.util.Comparator;
import java.util.ArrayList;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NonNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return new ArrayList<>(entities);
    }

    @Override
    public boolean exists(final String id) {
        return entities.stream()
                .filter(s -> id.equals(s.getId()))
                .findFirst().orElse(null) != null;
    }

    @Override
    public E add(final E entity) {
        if (entity == null) return null;
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear() {
        final List<E> entities = findAll();
        if (entities == null) return;
        this.entities.removeAll(entities);
    }

    @Override
    public void addAll(List<E> list) {
        entities.addAll(list);
    }

    @Override
    public void remove(final @NonNull E entity) {
//        entities.remove(entity.getId());
        entities.remove(entity);
    }
}
