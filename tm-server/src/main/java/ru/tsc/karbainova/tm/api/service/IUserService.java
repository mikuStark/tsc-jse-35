package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.User;

import java.util.List;

public interface IUserService {

    void clear();

    void addAll(List<User> users);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );

    User setPassword(String userId, String password);
}
