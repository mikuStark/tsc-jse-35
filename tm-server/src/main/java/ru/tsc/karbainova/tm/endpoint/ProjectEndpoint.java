package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.api.service.IProjectToTaskService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Session;
import ru.tsc.karbainova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint {

    private IProjectService projectService;
    private IProjectToTaskService projectToTaskService;
    private ServiceLocator serviceLocator;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(
            final ServiceLocator serviceLocator,
            final IProjectService projectService,
            final IProjectToTaskService projectToTaskService
    ) {
        this.serviceLocator = serviceLocator;
        this.projectService = projectService;
        this.projectToTaskService = projectToTaskService;
    }


//    @WebMethod
//    public List<Project> findAllProjectByUserId(
//            @WebParam(name = "userId") String userId,
//            @WebParam(name = "comparator") Comparator<Project> comparator
//        ) {
//        return projectService.findAll(userId, comparator);
//    }

    @WebMethod
    public Project findByIdProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @WebMethod
    public Project removeByIdProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeById(session.getUserId(), id);
    }

    @WebMethod
    public List<Project> findAllProject(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findAll();
    }

    @WebMethod
    public void createProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") @NonNull String name
    ) {
        serviceLocator.getSessionService().validate(session);
        projectService.create(session.getUserId(), name);
    }

    @WebMethod
    public void createProjectAllParam(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") @NonNull String name,
            @WebParam(name = "description") @NonNull String description) {
        serviceLocator.getSessionService().validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @WebMethod
    public void addProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "project") @NonNull Project project) {
        serviceLocator.getSessionService().validate(session);
        projectService.add(session.getUserId(), project);
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "project") @NonNull Project project) {
        serviceLocator.getSessionService().validate(session);
        projectService.remove(session.getUserId(), project);
    }

    @WebMethod
    public List<Project> findAllProjectByUs(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findAll(session.getUserId());
    }

    @WebMethod
    public Project updateByIndexProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") @NonNull Integer index,
            @WebParam(name = "name") @NonNull String name,
            @WebParam(name = "description") @NonNull String description) {
        serviceLocator.getSessionService().validate(session);
        return projectService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public Project removeByIndexProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") @NonNull Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project removeByNameProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") @NonNull String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return projectService.removeByName(session.getUserId(), name);
    }

    @WebMethod
    public Project findByNameProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") @NonNull String name) {
        serviceLocator.getSessionService().validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public List<Task> findTaskByProjectIdProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return projectToTaskService.findTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public Task taskBindByIdProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") @NonNull String projectId,
            @WebParam(name = "taskId") @NonNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return projectToTaskService.taskBindById(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public Task taskUnbindByIdProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") @NonNull String projectId,
            @WebParam(name = "taskId") @NonNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return projectToTaskService.taskUnbindById(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public void removeAllTaskByProjectIdProject(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        projectToTaskService.removeAllTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public Project removeByIdPro(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") @NonNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return projectToTaskService.removeById(session.getUserId(), projectId);
    }
}
